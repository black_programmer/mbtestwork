﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using forMBicycle.App_Data;
using forMBicycle.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using Newtonsoft.Json;

namespace forMBicycle.Controllers {
    [Route("api/[controller]")]
    public class StaffController : Controller {
        private DataContext db;

        /// <summary>
        /// Connect to Database
        /// </summary>
        /// <param name="db"></param>
        public StaffController(DataContext db) {
            this.db = db;
        }

        /// <summary>
        /// Import data form database for Staff page. 
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public ActionResult Index() {
            List<object> temp = new List<object>();
            var data = db.Employees.Include(p => p.Position).ToList();
            foreach (var i in data) {      
                temp.Add(new { Id = i.Id, FirstName = i.FirstName, LastName = i.LastName, Position = i.Position.PositionName });
            }
            return Json(temp);
        }

        /// <summary>
        /// Get positions
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public ActionResult Positions() {
            return Json(db.Positions.ToList());
        }

        /// <summary>
        /// Get position by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public ActionResult PositionById(int id) {
            return Json(db.Positions.Where(p => p.Id == id));
        }

        /// <summary>
        /// Get details for emoloyee by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public ActionResult Details(int id) {
            return Json(db.Employees.Where(p => p.Id == id));
        }

        /// <summary>
        /// Add new employee to database
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public ActionResult Add(Employee employee) {
            var data = db.Employees.Add(new Employee() {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                PositionId = employee.PositionId
            });
            db.SaveChangesAsync();
            return Json(data.State);
        }

        /// <summary>
        /// Update employee in database
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPut("[action]")]
        public ActionResult Edit(Employee employee) {
            var data = db.Employees.FirstOrDefault(item => item.Id == employee.Id);
            if (data != null) {
                data.Id = employee.Id;
                data.FirstName = employee.FirstName;
                data.LastName = employee.LastName;
                data.PositionId = employee.PositionId;
            }
            var result = db.Employees.Update(data);
            db.SaveChangesAsync();
            return Json(result.State);
        }

        /// <summary>
        /// Delete employee by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("[action]")]
        public ActionResult Delete(int id) {
            var temp = db.Employees.Remove(new Employee() { Id = id });
            db.SaveChanges();
            return Json(temp.State);
        }
    }
}