﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace forMBicycle.Models {
    public class Position {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string PositionName { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
