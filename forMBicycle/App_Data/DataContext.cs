﻿using forMBicycle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace forMBicycle.App_Data {
    public class DataContext : DbContext {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Position> Positions { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options) {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Position>().HasData(
                new Position { Id = 1, PositionName = "Senior" },
                new Position { Id = 2, PositionName = "Middle" },
                new Position { Id = 3, PositionName = "Junior" },
                new Position { Id = 4, PositionName = "QA" });

            modelBuilder.Entity<Employee>().HasData(
                new Employee { Id = 1, FirstName = "Иван", LastName = "Смирнов", PositionId = 1 },
                new Employee { Id = 2, FirstName = "Петр", LastName = "Попов", PositionId = 2},
                new Employee { Id = 3, FirstName = "Мария", LastName = "Васильева", PositionId = 3},
                new Employee { Id = 4, FirstName = "Степан", LastName = "Лебедев", PositionId = 4},
                new Employee { Id = 5, FirstName = "Дарья", LastName = "Козлова", PositionId = 3 },
                new Employee { Id = 6, FirstName = "Ольга", LastName = "Орлова", PositionId = 2 },
                new Employee { Id = 7, FirstName = "Олег", LastName = "Захаров", PositionId = 3 },
                new Employee { Id = 8, FirstName = "Иван", LastName = "Кузьмин", PositionId = 2 },
                new Employee { Id = 9, FirstName = "Иван", LastName = "Фролов", PositionId = 4 },
                new Employee { Id = 10, FirstName = "Александр", LastName = "Беляев", PositionId = 4 });
        }
    }
}
