﻿import { Component, Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { AppService } from "./app.service";

interface Employees {
    Id: Number,
    FirstName: String,
    LastName: String,
    PositionId: String
}
interface Positions {
    Id: Number,
    PositionName: String
}

@Component({
    moduleId: module.id,
    selector: 'store-service'
})
@Injectable()
export class StoreService {
    private selected: Number = 1; // Data selected

    constructor(private appService: AppService) {
    }

    //Set data
    setSelected(id: Number) {
        this.selected = id;
    }

    //Get data
    getSelected() {
        return this.selected;
    }
}