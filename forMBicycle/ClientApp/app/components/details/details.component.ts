﻿import { Component, Output, Input, Inject } from "@angular/core"
import { AppService } from "../app.service";
import { ActivatedRoute, Router } from "@angular/router";
import { StoreService } from "../store.service";
import * as $ from 'jquery';

interface Employees {
    Id: Number,
    FirstName: String,
    LastName: String,
    PositionId: String
}
interface Positions {
    Id: Number,
    PositionName: String
}

@Component({
    selector: 'details-component',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.css']
})
export class DetailsComponent {
    public employees: Employees[] | undefined;  // List employees for Staff page
    public positions: Positions[] | undefined;  // List positions for Add new employee

    constructor(private appService: AppService, private storeService: StoreService, private activateRoute: ActivatedRoute, private router: Router) {
    }

    //It's event for load data after initialization page
    public ngOnInit() {
        this.appService.getEmployeeById(this.activateRoute.snapshot.params['id']).subscribe(res => {
            this.employees = res.json() as Employees[];
        }, err => { console.log(err) });
        this.getPositions();
    }

    //Update employee in database
    saveEmployee(id: Number, fName: String, lName: String) {
        this.appService.editEmployee(id, fName, lName, this.storeService.getSelected());
        this.router.navigate(['staff']);
        document.location.reload(true);
    }

    //Get positions
    getPositions() {
        this.appService.getPositions().subscribe(res => {
            this.positions = res.json() as Positions[];
        }, err => console.log(err));
    }

    //Event for save data selected
    selectChangeHandler(event: any) {
        this.storeService.setSelected(event.target.value);
    }

    //Get data selected
    getSelected() {
        return this.storeService.getSelected();
    }

    //Enable input for edit
    EditEnable() {
        $('.editInput').prop('disabled', false);
    }
}