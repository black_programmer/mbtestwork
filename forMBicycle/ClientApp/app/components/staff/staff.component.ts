﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import { StoreService } from '../store.service';

interface Employees {
    Id: Number,
    FirstName: String,
    LastName: String,
    Position: String
}
interface Positions {
    Id: Number,
    PositionName: String
}

@Component({
    selector: 'staff-component',
    templateUrl: './staff.component.html',
    styleUrls: ['./staff.component.css']
})
export class StaffComponent {
    public employees: Employees[] | undefined;  // List employees for Staff page
    public positions: Positions[] | undefined;  // List positions for Add new employee

    constructor(private appService: AppService, private storeService: StoreService, private router: Router) {
    }

    //It's event for load data after initialization page
    ngOnInit() {
        this.appService.getEmployees().subscribe(result => {
            this.employees = result.json() as Employees[];
        }, err => { alert('Error!') });
        this.getPositions();
    }

    //Add new employee
    add(fName: String, lName: String) {
        this.appService.addEmployee(fName, lName, this.storeService.getSelected());
        document.location.reload(true);
    }

    //Delete employee
    delete(id: Number) {
        this.appService.deleteEmployee(id);
        document.location.reload(true);
    }

    //Get positions
    getPositions() {
        this.appService.getPositions().subscribe(res => {
            this.positions = res.json() as Positions[];
        }, err => console.log(err));
    }

    //Event for save data selected
    selectChangeHandler(event: any) {
        this.storeService.setSelected(event.target.value);
        console.log(this.storeService.getSelected());
    }
}
