﻿import { Component, Injectable, Inject } from "@angular/core";
import { Http } from "@angular/http";

interface Employees {
    Id: Number,
    FirstName: String,
    LastName: String,
    PositionId: String
}
interface Positions {
    Id: Number,
    PositionName: String
}

@Component({
    moduleId: module.id,
    selector: 'app-service'
})
@Injectable()
export class AppService {
    private uri: string | undefined;
    private selected: any;

    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string) {
        this.uri = baseUrl;
    }

    //It's Http requests for Angular application

    //get position by id
    getPositionById(id: Number) {
        return this.http.get(this.uri + `api/Staff/PositionById/?id=${id}`);
    }

    //get positions
    getPositions() {
        return this.http.get(this.uri + `api/Staff/Positions`);
    }

    //get employee by id
    getEmployeeById(id: Number) {
        return this.http.get(this.uri + `api/Staff/Details/?id=${id}`);
    }

    //get employee
    getEmployees() {
        return this.http.get(this.uri + 'api/Staff/Index');
    }

    //add new employee
    addEmployee(fName: String, lName: String, positionId: Number) {
        this.http.post(this.uri + `api/Staff/Add/?firstName=${fName}&lastName=${lName}&positionId=${positionId}`, {}).subscribe();
    }

    //update employee
    editEmployee(id: Number, fName: String, lName: String, positionId: Number) {
        this.http.put(this.uri + `api/Staff/Edit/?id=${id}&firstName=${fName}&lastName=${lName}&positionId=${positionId}`, {}).subscribe();
    }

    //delete employee by id
    deleteEmployee(id: Number) {
        this.http.delete(this.uri + `api/Staff/Delete/?id=${id}`).subscribe();
    }
}