﻿// Модули
import { NgModule } from '@angular/core';
import { AppService } from './app.service';
import { StoreService } from './store.service';

@NgModule({
    declarations: [],
    exports: [],
    providers: [AppService, StoreService]
})
export class AppModule { }